pyyaml==6.0.*
cerberus==1.3.*
requests==2.31.*
docker==6.1.*
colorlog==6.8.*
GitPython==3.1.*
pytest==7.4.*
